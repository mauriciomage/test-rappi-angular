import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';

@NgModule({
  declarations: [
    CategoriesComponent
  ],
  imports: [
    BrowserModule,
    CategoriesRoutingModule
  ],
  providers: [],
  bootstrap: [CategoriesComponent]
})
export class CategoriesModule { }
