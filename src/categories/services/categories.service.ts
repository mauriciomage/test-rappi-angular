import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class CategoriesService {

  constructor(private httpService: HttpClient) {
  }

  getContentJSON() {
    return this.httpService.get('../assets/data/categories.json').pipe(map(data => data));
  }
}
