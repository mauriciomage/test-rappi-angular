import { Component, OnInit } from '@angular/core';
import { CategoriesService } from './services/categories.service';
import { ProductsService } from '../products/services/products.service';
import * as _ from 'lodash';

@Component({
  selector: 'abe-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  title = 'angular-test';
  categories: any[] = [];
  sublevels: any[] = [];
  category = {};
  products: any[] = [];
  constructor(
    public catService: CategoriesService,
    private proService: ProductsService
  ) {}

    ngOnInit() {
      this.getCategories();
    }

    private getCategories() {
      this.catService.getContentJSON().subscribe(result => {
        this.categories = _.get(result, 'categories');
      });
    }

    private filter(e) {
      this.proService.getContentJSON().subscribe(result => {
        this.products = _.get(result, 'products');

        const obj = new CategoriesComponent(this.catService, this.proService);
        const categorias = this.categories;
        this.products =  _.map(this.products, function(product) {
          const sublevel_id = product.sublevel_id;
          const sublevel = obj.searhSubLevelId(categorias, sublevel_id, 0);
          return _.assign(product, {sublevel: _.get(sublevel, 'name')});
        });
        this.proService.filterProductByCategory(this.products, _.get(e, 'target.id'));
        console.log('productos filtrados', this.proService.products);
      });
    }

    searhSubLevelId(data, id: number, levelForSearch: number) {
      levelForSearch++;
      for (let i = 0; i < data.length; i++) {
        const element = data[i];
          if (element.id === id && levelForSearch !== 1) {
            return element;
          } else {
              if (element.sublevels) {
                const devolver = this.searhSubLevelId(element.sublevels, id, levelForSearch);
                if (devolver) {
                  return devolver;
                }
              }
           }
      }
    }
}
