import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'abe-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  title = 'mostrar carrito';
  @Input() shoppingCartCounter;
  @Input() cart: any[] = [];
  showTable: Boolean = false;
  quantityExited: string;
  constructor() {}

    ngOnInit() {
        this.getShoppingCart();
    }

    private getShoppingCart() {
        this.cart = JSON.parse(localStorage.getItem('shopping_cart'));
        this.shoppingCartCounter = JSON.parse(localStorage.getItem('shopping_cart')).length;
    }

    private viewCart() {
        if (this.showTable) {
            this.showTable = false;
            this.title = 'mostrar carrito';
        } else {
            if (this.shoppingCartCounter > 0) {
                this.showTable = true;
                this.title = 'cerrar carrito';
            }
        }
    }

    private dropCart(product) {
        this.cart = JSON.parse(localStorage.getItem('shopping_cart'));
        _.remove(this.cart, function(item) {
            return item.id  === product.id;
          });
          localStorage.removeItem('shopping_cart');
          localStorage.setItem('shopping_cart', JSON.stringify(this.cart));
          this.shoppingCartCounter = JSON.parse(localStorage.getItem('shopping_cart')).length;
    }

    private changeQty(req, id, shoppingCart) {
        this.cart = _.map(shoppingCart, function(item) {
            if (item.id === id) {
                return _.assign(item, {quantity: req.value.quantityProduct});
            }
        });
        localStorage.removeItem('shopping_cart');
        localStorage.setItem('shopping_cart', JSON.stringify(this.cart));
        this.showTable = false;
    }
}
