import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CartRoutingModule } from './cart-routing.module';
import { CartComponent } from './cart.component';

@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    BrowserModule,
    CartRoutingModule
  ],
  providers: [],
  bootstrap: [CartComponent]
})
export class CartModule { }
