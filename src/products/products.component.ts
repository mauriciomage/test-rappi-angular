import { Component, OnInit, Input } from '@angular/core';
import { ProductsService } from './services/products.service';
import { CategoriesService } from '../categories/services/categories.service';
import { CartComponent } from '../cart/cart.component';
import * as _ from 'lodash';

@Component({
  selector: 'abe-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  title = 'angular-test';
  @Input() products: any[] = [];
  categories: any[] = [];
  ordered: Boolean = false;
  order: string;
  filtered: Boolean = false;
  filter: string;
  filterByEnabled: Boolean = false;

  priceStart: string;
  priceFnish: string;

  shoppingCart: any[] = [];
  shoppingCartCounter: Number = 0;
  cart: string;
  constructor(
    public proService: ProductsService,
    public catService: CategoriesService,
  ) {}

    ngOnInit() {
      this.getProducts();
    }

    getProducts() {
      this.catService.getContentJSON().subscribe(result => {
        this.categories = _.get(result, 'categories');
      });
      this.proService.getContentJSON().subscribe(result => {
        this.products = _.get(result, 'products');
        this.mapProducts(this.products);
      });
    }

    searhSubLevelId(data, id: number, levelForSearch: number) {
      levelForSearch++;
      for (let i = 0; i < data.length; i++) {
        const element = data[i];
          if (element.id === id && levelForSearch !== 1) {
            return element;
          } else {
              if (element.sublevels) {
                const devolver = this.searhSubLevelId(element.sublevels, id, levelForSearch);
                if (devolver) {
                  return devolver;
                }
              }
           }
      }
    }

    private orderBy(e) {
      this.ordered = true;
      this.order = _.get(e, 'target.innerText');
      if (this.order  === 'Precio') {
        this.products = _.orderBy(this.products, ['price'], ['asc']);
      } else
      if (this.order  === 'Disponibilidad') {
        this.products = _.orderBy(this.products, ['available'], ['asc']);
      } else
      if (this.order  === 'Cantidad') {
        this.products = _.orderBy(this.products, ['quantity'], ['asc']);
      }
    }

    private mapProducts(products) {
      const categorias = this.categories;
      const obj = new ProductsComponent(this.proService, this.catService);
      this.products =  _.map(products, function(product) {
        const sublevel_id = product.sublevel_id;
        const sublevel = obj.searhSubLevelId(categorias, sublevel_id, 0);
        return _.assign(product, {sublevel: _.get(sublevel, 'name')});
      });
    }

    private reload() {
      this.ordered = false;
      this.filtered = false;
      this.getProducts();
    }

    private filterUnavailables(e) {
      this.filtered = true;
      this.filter = _.get(e, 'target.innerText');
      if (this.filter === 'No Disponibles') {
        this.products = _.filter(this.products, {'available': false});
      } else {
        this.products = _.filter(this.products, {'available': true});
      }
      this.filtered = true;
    }

    private filterPrice(req) {
      this.products =  _.map(this.products, function(product) {
        let price = product.price.replace(',', '');
        price = parseInt(price.substring(1), null);
          return _.assign(product, {priceNumber: price});
      });
      this.products = _.filter(this.products, function(o) {return o.priceNumber > req.value.priceStart
        &&  o.priceNumber < req.value.priceFinish ; });
        this.filtered = true;
        this.filter = 'Precio';
    }

    private addCart(product) {
      if (!localStorage.getItem('shopping_cart')) {
        this.shoppingCart.push(product);
        localStorage.setItem('shopping_cart', JSON.stringify(this.shoppingCart));
      } else {
        this.shoppingCart = JSON.parse(localStorage.getItem('shopping_cart'));
        if (!_.find(this.shoppingCart, function(o) { return o.id === product.id; })) {
          this.shoppingCart.push(product);
          localStorage.setItem('shopping_cart', JSON.stringify(this.shoppingCart));
        }
      }
      this.shoppingCartCounter = this.shoppingCart.length;
      this.cart = JSON.parse(localStorage.getItem('shopping_cart'));
    }
}
