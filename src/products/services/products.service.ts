import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import { Subject } from 'rxjs/internal/Subject';

@Injectable()
export class ProductsService {
  products: any[] = [];
  productsChange: Subject<[]> = new Subject<[]>();

  constructor(private httpService: HttpClient) {
    this.productsChange.subscribe((value) => {
      this.products = value;
    });
  }

  getContentJSON() {
    return this.httpService.get('../assets/data/products.json').pipe(map(data => data));
  }

  filterProductByCategory(products, filterCategory) {
    this.products = _.filter(products, {'sublevel': filterCategory});
  }
}
